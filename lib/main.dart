
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';

void main() => runApp(LogoApp());
bool image = true;
class AnimatedLogo extends AnimatedWidget {
  static final _opacityTween= Tween(begin:0, end: 1);
  static final _sizeTween = Tween<double>(begin:0, end: 300);


  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);
  Widget SelectImage(){

    if(image ==true)
      {

        return Image.asset('assets/images/Skyeevee.png');
      }
    else
      {
        return Image.asset('assets/images/melted.png');
      }

  }
  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Center(
      child: RotationTransition(
        turns: animation,

      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10),
        height: _sizeTween.evaluate(animation),
        width:  _sizeTween.evaluate(animation),
        child: SelectImage(),
        //Image.asset('assets/images/Skyeevee.png'),
      ),

      )
    );
  }
}

class LogoApp extends StatefulWidget {
  _LogoAppState createState() => _LogoAppState();
}

// #docregion print-state
class _LogoAppState extends State<LogoApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;


  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 3), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.fastOutSlowIn)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reverse();

        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
          if (image) {
            image = false;
          }
          else
            {
              image = true;
            }
        }
      })

      ..addStatusListener((state) => print('$state'));
    controller.forward();
  }


  @override
  Widget build(BuildContext context) => AnimatedLogo(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

}
